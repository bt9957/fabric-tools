# install curl
apt-get update && apt-get --yes --force-yes install curl

# downloads docker
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh

docker --version

# download docker-compose
curl -L https://github.com/docker/compose/releases/download/1.22.0-rc2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


